let grades = [98.5, 94.3, 89.2, 90.0];

console.log(grades);
	
let grade = {
	math: 98.5,
	english:94.3,
	science:90.0,
	MAPEH:89.2
};

console.log(grade)

/*
	ACCESSING ELEMENTS INSIDE THE OBJECT
		SYNTAX FOR CREATING AN OBJECT
			let objectName = {
				keyA: valueA,
				keyB: valueB
			}

	Dot Notation - used to access the specific property and value of the object, preferred in terms of object access
*/
console.log(grade.english);
// accessing two keys in the object
console.log(grade.english, grade.math);

/*
	Create a cellphone object that has the following keys (supply your own values for each)
*/

let cellphone = {
	brand: "iPhone",
	color: "green",
	MFD: 2021
}
console.log(cellphone)
// typeof determines the data type of the element
console.log(typeof cellphone);

let student = {
	firstName: "John",
	lastName: "Smith",
	mobileNo: 09123456789,
	location:{
		city: "Tokyo",
		country: "Japan"
	},
	email: ["john@mail.com", "john.smith@mail.com"],

	fullName: (function name(fullName){
		//this - refers to the object where it is inserted.
		/*
			in this case, the code below could be typed as 
				console.log(student.firstNamne + " " + student.lastName);

		*/
		console.log(this.firstName + " " + this.lastName);
	})
}

//console.log(student.firstNamne + " " + student.lastName);

student.fullName();

//Array of Objects
let contactList = [
{
	firstName:"John",
	lastName:"Smith",
	location:"Japan"
},
{
	firstName:"Jane",
	lastName:"Smith",
	location:"Japan"
},
{
	firstName:"Jasmin",
	lastName:"Smith",
	location:"Japan"
}
];

console.log(contactList[1]);
console.log(contactList[2].firstName);

/*
	Constructor Function
	instance - concrete occurence of any object which emphasizes on the unique identity of it
*/

//Reusable Function/ Object Method
function laptop(name, manufacturedDate){
	this.name = name,
	this.manufacturedDate = manufacturedDate
};

//new kyeword refers/signifies that there will be a new object under the laptop
let laptop1 = new laptop("Lenovo", 2008);
console.log(laptop1);

let laptop2 = new laptop("Toshiba", 1997);
console.log(laptop2);

/*create a variable and store the laptop1 and laptop2 in it(the resulting output must be an array.*/

let array = [laptop1, laptop2];
console.log(array);

/*
initializing, adding, deleting, reassigning of object properties
*/

let car = {};
console.log(car);
//adding object properties

car.name="Honda Civic";
console.log(car);

car["manufacturedDate"] = 2020;
console.log(car);

//reassigning object(same/existing properties, but assigning a different value)
car.name = "Volvo";
console.log(car);

//deleting object properties
/*delete car.manufacturedDate;
console.log(car);*/

delete car["manufacturedDate"];
console.log(car);

let person = {
	name: "John",
	talk: function(){
		console.log('Hi! My name is ' + this.name)
	}
}

//add property
person.walk = function(){
	console.log(this.name + " walked 25 steps forward.");
}

let friend = {
	firstName: "Joe",
	lastName: "Doe",
	address:{
		city: "Austin",
		state: "Texas"
	},
	emails:["joe@mail.com", "joedoe@mail.com"],
	introduce: function(){
		console.log("Hello! My name is "+ this.firstName + " " + this.lastName)
	}
};

let pokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon.");
		console.log("targetPokemon's health reduced to targetPokemonHealth.");
	}
};

function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2*level,
	this.attack = level
	// Skills of Pokemon
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	}
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}
let Pikachu = new Pokemon ("Pikachu", 16);
let Charizard = new Pokemon("Charizard", 8);
let Balbasaur = new Pokemon("Balbausar", 9);
let Raichu = new Pokemon("Raichu", 10);