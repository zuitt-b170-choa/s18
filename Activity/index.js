let user = {
	name: "Gab",
	friends: ["Vina", "Ony", "Cyreen"],
	age: 22,
	champion: ["Ryze", "Brand", "Ahri", "Lux"],
	talk: function(element){
		if(element === 0){
		console.log(this.champion[element] +" attack!");
		}
		if(element > 0){
			console.log(this.champion[element-1] + ", you can rest for now.");
			console.log(this.champion[element] +" attack!");
		}
	},
	win: function(){
		console.log("Whoo! We did it! We can now go back to Runeterra and rest.")
	}
};

console.log("Result of dot notation:")
console.log(user.name);
console.log("Result of square bracket notation:");
console.log(user['champion'])
user.talk(0);


function Champion(name, level){
	this.name = name,
	this.level = level,
	this.health = 20*level,
	this.attack = level*10,
	this.hit = function(target){
		target.health = target.health - this.attack;
		console.log(this.name + " hit " + target.name);
		console.log(target.name + "'s health is now reduced to " + target.health)
		console.log(target);
		if(target.health <= 0){
		target.dead();
		console.log(target);
			}
	},
	this.dead = function(){
		console.log(this.name + " is dead.")
	}
}

let Ryze = new Champion("Ryze", 10);
let Brand = new Champion("Brand", 7);
let Ahri = new Champion("Ahri", 9);
let Lux = new Champion("Lux", 12);

let Garen = new Champion("Garen", 13);
let Shivana = new Champion("Shivana", 8);
let TwistedFate = new Champion("Twisted Fate", 5)

Ryze.hit(Garen);
Garen.hit(Ryze);
Ryze.hit(Garen);
Garen.hit(Ryze);

user.talk(1);

Garen.hit(Brand);
Brand.hit(Garen);

Brand.hit(Shivana);
Shivana.hit(Brand);

user.talk(2);

Shivana.hit(Ahri);
Ahri.hit(Shivana);

Ahri.hit(TwistedFate);
TwistedFate.hit(Ahri);
Ahri.hit(TwistedFate);

user.win();